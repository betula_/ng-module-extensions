
module.directive('pressEnter', [function() {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			element.bind('keydown', function(e) {
				if (e.keyCode === 13) {
					scope.$evalAsync(attrs.pressEnter);
				}
			});
		}
	}
}]);