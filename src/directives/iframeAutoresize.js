module.directive('iframeAutoresize', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.width) {
					element.width(data.width);
				}
				if (data.height) {
					element.height(data.height);
				}
			});
		}
	}
}]);

module.directive('iframeAutoresizeWidth', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.width) {
					element.width(data.width);
				}
			});
		}
	}
}]);

module.directive('iframeAutoresizeHeight', [function() {
	return {
		restrict: 'A',
		require: 'iframeCommunication',
		link: function(scope, element, attrs, comm) {
			comm.on('resize', function(data) {
				if (!data) {
					return;
				}
				if (data.height) {
					element.height(data.height);
				}
			});
		}
	}
}]);