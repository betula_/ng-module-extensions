
module.filter('range', function() {
	// TODO: Change to (repeat-range="A..B") because it refresh every digest loop
	return function(input) {
		var low = 0;
		var high = 0;
		if (typeof input.length == 'undefined') {
			low = 1;
			high = parseInt(input);
		}
		else if (input.length == 1) {
			high = parseInt(input[1]);
		}
		else if (input.length == 2) {
			low = parseInt(input[1]);
			high = parseInt(input[2]);
		}
		var range = [];
		var i;
		for (i = low; i <= high; i++) {
			range.push(i);
		}
		return range;
	};
});