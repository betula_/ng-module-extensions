module.directive('a', [function() {
	return {
		restrict: 'E',
		compile: function(element) {
			if (!element.attr('href')) {
				element.attr('href', '');
				element.on('click', function(e) {
					e.preventDefault();
				});
			}
		}
	}
}]);