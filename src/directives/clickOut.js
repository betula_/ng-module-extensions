
module.directive('clickOut', ['$browser', function($browser) {
	return {
		restrict: 'A',
		link: function($scope, $element, $attrs) {
			var target;
			$element.on('click', function(event) {
				target = event.target;
				$browser.defer(function() {
					target = undefined;
				});
			});
			var bodyClickListener = function(event) {
				if (target !== event.target) {
					$scope.$evalAsync($attrs.clickOut);
				}
			};

			var body = angular.element(document.getElementsByTagName('body'));
			$browser.defer(function() {
				body.on('click', bodyClickListener);
			});
			$scope.$on('$destroy', function() {
				body.off('click', bodyClickListener);
			});
		}
	}
}]);