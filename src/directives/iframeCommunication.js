module.directive('iframeCommunication', [function() {

	return {
		restrict: 'A',
		controller: ['$element', '$attrs', 'EventEmitterFactory', function($element, $attrs, EventEmitterFactory) {

			EventEmitterFactory.call(this);

			var getUrlHost = function(url, def) {
				return (/^[a-z0-9]+:[/]{2}[^/]+/.exec(url) || [])[0] || def || url;
			};
			var REGISTER_OWN_TARGET_TIMEOUT = 334;
			var MESSAGE_KEY = '::IFrameCommunication:5d41402abc4b2a76b9719d911017c592';

			var unbindDelegate;
			this.unbind = function() {
				unbindDelegate && unbindDelegate();
				unbindDelegate = null;
			};

			var ownTarget = getUrlHost(window.location.href);
			this.bind = function() {
				connect();
				var unbindTargetOriginObserver = $attrs.$observe('iframeOrigin', function() {
					connect();
				});

				var self = this;
				var messageListener = function(event) {
					var message = event.data;
					try {
						message = (JSON.parse(message) || {})[MESSAGE_KEY];
						if (message && message.action && typeof message.action == 'string') {
							self.emit(message.action, message.data);
						}
					}
					catch (e) {}
				};

				window.addEventListener('message', messageListener);
				var unbindMessageListener = function() {
					window.removeEventListener('message', messageListener);
				};

				var stopConnectingAttempts;
				function connect() {
					stopConnecting();
					var timerId = null;
					stopConnectingAttempts = function() {
						clearTimeout(timerId);
					};
					attempt();
					function attempt() {
						clearTimeout(timerId);
						timerId = setTimeout(function() {
							self.send('connect', ownTarget);
							attempt();
						}, REGISTER_OWN_TARGET_TIMEOUT);
					}
				}
				function stopConnecting() {
					stopConnectingAttempts && stopConnectingAttempts();
				}
				var unbindReadyListener = self.on('ready', function() {
					stopConnecting();
				});

				unbindDelegate = function() {
					unbindTargetOriginObserver();
					unbindMessageListener();
					stopConnecting();
					unbindReadyListener();
				};
			};

			var getTargetOrigin = function() {
				return getUrlHost($attrs.iframeOrigin, ownTarget);
			};

			this.send = function(action, data) {
				var window = $element[0].contentWindow;
				if (window && window.postMessage) {
					window.postMessage(JSON.stringify({
						action: action,
						data: data,
						key: MESSAGE_KEY
					}), getTargetOrigin());
				}
			};

		}],
		link: function(scope, element, attrs, ctrl) {
			element.on('load', function() {
				ctrl.unbind();
				ctrl.bind();
			});
		}
	}
}]);