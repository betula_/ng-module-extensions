
module.directive('formAjaxAutosubmit', ['$browser', 'jQuery', '$timeout', function($browser, jQuery, $timeout) {
	return {
		restrict: 'A',
		require: 'formAjax',
		compile: function() {
			return {
				post: function(scope, element, attrs, formAjax) {
					var getFormData = formAjax.getData;
					var delayTime = parseInt(attrs.formAjaxAutosubmit) || 2000;

					var last;
					var timeoutId;
					var submit = function(data) {
						timeoutId = null;
						var options = { isAutosubmit: true };
						if (data) options.data = data;
						element.trigger('submit', options);
					};

					var unwatch;
					$timeout(function() {
						unwatch = scope.$watch(getFormData, function(curr, prev) {
							if (curr === prev) return;
							last = curr;
							$browser.defer.cancel(timeoutId);
							timeoutId = $browser.defer(submit, delayTime);
						}, true);
					}, 500);

					var windowElement = jQuery(window);
					var leave = function() {
						windowElement.unbind('unload', leave);
						$browser.defer.cancel(timeoutId);
						if (unwatch) unwatch();
						if (timeoutId && last) submit(last);
					};
					windowElement.bind('unload', leave);
					scope.$on('$destroy', leave);
				}
			}
		}
	}
}]);