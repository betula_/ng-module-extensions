
module.directive('pressEnterSubmit', [function() {
	return {
		restrict: 'A',
		require: '^form',
		link: function ($scope, $element, $attrs, form) {
			$element.bind('keydown', function(e) {
				if (e.keyCode === 13) {
					$element.trigger('submit');
				}
			});
//				var element = $element;
//				var formElement;
//				while (element.length) {
//					element = element.parent();
//					if (element.data('$ngFormController') === form) {
//						formElement = element;
//						break;
//					}
//				}
//				if (formElement) {
//					$element.bind('keydown', function(e) {
//						if (e.keyCode === 13) {
//							formElement.trigger('submit');
//						}
//					});
//				}
		}
	}
}]);