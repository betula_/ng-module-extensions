
module.directive('deferredIf', ['$timeout', function($timeout) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var promise;
			scope.$watch(attrs.deferredIf, function(value) {
				$timeout.cancel(promise);
				if (value) {
					promise = $timeout(function() {
						if (scope.$eval(attrs.deferredIf)) {
							scope.$eval(attrs.deferredIfExpression);
						}
					}, attrs.deferredIfTimeout);
				}
				else {
					scope.$eval(attrs.deferredIfElse);
				}
			});
			scope.$on('$destroy', function() {
				$timeout.cancel(promise);
			});
		}
	}
}]);