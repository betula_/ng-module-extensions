
module.directive('focus', ['jQuery', function(jQuery) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if (attrs.focus) {
				scope.$watch(attrs.focus, function(focus) {
					if (focus) {
						scope.$$postDigest(function() {
							jQuery(element).focus();
						});
					}
				});
			}
			else {
				scope.$$postDigest(function() {
					jQuery(element).focus();
				});
			}

		}
	}
}]);
