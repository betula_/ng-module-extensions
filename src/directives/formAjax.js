
module.directive('formAjax', ['$http', 'jQuery', function($http, jQuery) {
	return {
		restrict: 'A',
		require: ['form', 'formAjax'],
		controller: ['$scope', '$element', '$attrs', 'EventEmitterFactory', '$location', function($scope, $element, $attrs, EventEmitterFactory, $location) {
			var form = $element.controller('form');
			EventEmitterFactory(this);

			this.form = form;
			this.getData = function() {
				var data = {};
				if ($attrs.formAjaxData) {
					data = Object($scope.$eval($attrs.formAjaxData));
				}
				else {
					Object.keys(form).forEach(function(name) {
						if (Object(form[name]).hasOwnProperty('$modelValue')) {
							data[name] = form[name].$modelValue;
						}
					});
				}
				if ($attrs.formAjaxAppend) {
					var appendedData = Object($scope.$eval($attrs.formAjaxAppend));
					Object.keys(appendedData).forEach(function(name) {
						data[name] = appendedData[name];
					});
				}
				if ($attrs.formAjaxRequest) {
					var requestedData = $scope.$eval($attrs.formAjaxRequest, {
						$data: data
					});
					if (requestedData) {
						data = requestedData;
					}
				}
				return data;
			};
		}],
		link: function(scope, element, attrs, ctrls) {
			var form = ctrls[0];
			var formAjax = ctrls[1];
			var getFormData = formAjax.getData;

			var pending = {
				counter: 0,
				start: function() {
					this.counter ++;
					form.$submitting = true;
				},
				finish: function() {
					if (-- this.counter == 0) {
						form.$submitting = false;
					}
				}
			};

			(function($setPristine) {
				form.$setPristine = function() {
					$setPristine.call(form);
					delete form.$submitted;
					delete form.$isSubmitError;
					delete form.$isSubmitSuccess;
					delete form.$submitting;
				};
			})(form.$setPristine);

			element.on('submit', function(event, options) {
				options = options || {};
				event.preventDefault();
				form.$submitted = true;

				var eventData = {
					$form: form
				};
				if (attrs.formAjaxSubmit) {
					scope.$eval(attrs.formAjaxSubmit, eventData);
				}
				formAjax.emit('submit', eventData);

				if (form.$invalid) {
					if (!scope.$root.$$phase) scope.$digest();
					return;
				}

				var url = attrs.action || attrs.formAjax;
				var method = String(attrs.method || attrs.formAjaxMethod).toLowerCase();
				if (['get', 'post', 'put', 'delete', 'head', 'jsonp'].indexOf(method) == -1) method = 'post';

				var query = attrs.formAjaxQuery && scope.$eval(attrs.formAjaxQuery) || {};
				var data = options.data || getFormData();
				var promise;
				if (attrs.formAjaxPerform) {
					promise = scope.$eval(attrs.formAjaxPerform, {
						$method: method,
						$url: url,
						$data: data,
						$query: query
					});
				}
				else {
					var config = {
						url: url,
						method: method
					};
					if (method == 'get') {
						Object.keys(query).forEach(function(key) {
							if (!data.hasOwnProperty(key)) {
								data[key] = query[key];
							}
						});
						config.params = data;
					} else {
						config.params = query;
						config.data = data;
					}
					promise = $http(config);
				}

				if (attrs.formAjaxResponse) {
					var newPromise = scope.$eval(attrs.formAjaxResponse, {
						$method: method,
						$action: url,
						$query: query,
						$request: data,
						$promise: promise
					});
					if (newPromise) {
						promise = newPromise;
					}
				}

				pending.start();
				promise.then(function(response) {
					pending.finish();
					form.$isSubmitError = false;
					form.$isSubmitSuccess = true;

					var eventData = {
						$request: data,
						$response: response,
						$data: response.data
					};
					if (attrs.formAjaxSuccess) {
						scope.$eval(attrs.formAjaxSuccess, eventData);
					}
					formAjax.emit('success', eventData);

				}, function(response) {
					pending.finish();
					form.$isSubmitError = true;
					form.$isSubmitSuccess = false;

					var eventData = {
						$request: data,
						$response: response,
						$data: response.data
					};
					if (attrs.formAjaxError) {
						scope.$eval(attrs.formAjaxError, eventData);
					}
					formAjax.emit('error', eventData);

					if (form.$submitting) return;

					if (response.data && response.data.errors && response.data.errors.length) {
						response.data.errors.forEach(function(error) {
							var key = error.path;
							var name = error.code;
							if (Object(form[key]).hasOwnProperty('$modelValue')) {
								var ctrl = form[key];
								if (ctrl.$modelValue != data[key]) return;

								ctrl.$setValidity(name, false);

								var validator = function(value) {
									ctrl.$setValidity(name, true);
									var i;
									for (i = 0; i < ctrl.$parsers.length; ) {
										if (ctrl.$parsers[i] === validator) {
											ctrl.$parsers.splice(i, 1);
										} else ++i;
									}
									for (i = 0; i < ctrl.$formatters.length; ) {
										if (ctrl.$formatters[i] === validator) {
											ctrl.$formatters.splice(i, 1);
										} else ++i;
									}
									return value;
								};
								ctrl.$parsers.push(validator); // TODO: It should be executed first
								ctrl.$formatters.push(validator); // TODO: It should be executed first
							}
						});
					}

				});
				if (!scope.$root.$$phase) scope.$digest();
			});
		}
	}
}]);