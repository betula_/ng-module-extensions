
module.directive('textlimit', [function() {
	function isEmpty(value) {
		return typeof value == 'undefined' || value === '' || value === null || value !== value;
	}
	return {
		require: 'ngModel',
		restrict: 'A',
		link: function(scope, element, attrs, ctrl) {
			attrs.$set('maxlength', attrs.textlimit);
			ctrl.$parsers.unshift(function(viewValue) {
				if (isEmpty(viewValue) || typeof viewValue != 'string' || !attrs.textlimit || viewValue.length <= attrs.textlimit) {
					return viewValue;
				}
				viewValue = viewValue.substring(0, attrs.textlimit);
				ctrl.$viewValue = viewValue;
				ctrl.$render();
				return viewValue;
			});
		}
	}
}]);