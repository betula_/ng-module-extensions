
module.directive('body', [function() {
	return {
		restrict: 'E',
		controller: ['$scope', function($scope) {
			if (document.cookie.indexOf('--js-debug-ng') >= 0) {
				var totalLoopCount = 0;
				var digestCount = 1;
				var digestLoopCount = {};
				$scope.$watch(function() {
					totalLoopCount ++;

					if (!digestLoopCount[digestCount]) digestLoopCount[digestCount] = 0;
					digestLoopCount[digestCount] ++;

					console.log('Digest: '+digestCount, 'Loops in digest: '+digestLoopCount[digestCount], 'Total loops: '+totalLoopCount);

					var postRegistered = false;
					var i;
					for (i = 0; i < $scope.$$postDigestQueue.length; i++) {
						if ($scope.$$postDigestQueue[i] === postDigest) {
							postRegistered = true;
							break;
						}
					}
					if (!postRegistered) {
						$scope.$$postDigest(postDigest);
					}
				});
				function postDigest() {
					digestCount ++;
				}
			}
		}]
	}
}]);