module.filter('prettyBytes', function() {

	/*
	 * pretty-bytes
	 * Convert bytes to a human readable string: 1337 → 1.34 kB
	 * https://github.com/sindresorhus/pretty-bytes
	 * by Sindre Sorhus
	 * MIT License
	 */

	var isNaN = function (val) {
		return val !== val;
	};

	return function (num) {
		if (typeof num !== 'number' || isNaN(num)) {
			return num;
		}

		var exponent;
		var unit;
		var neg = num < 0;

		if (neg) {
			num = -num;
		}

		if (num === 0) {
			return '0 B';
		}

		exponent = Math.floor(Math.log(num) / Math.log(1000));
		num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
		unit = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'][exponent];

		return (neg ? '-' : '') + num + ' ' + unit;
	};
});






