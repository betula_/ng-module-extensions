
module.factory('$sessionStorageStore', ['$sessionStorage', function($sessionStorage) {
	return {
		get: function(key) {
			var value = $sessionStorage[key];
			return value ? angular.fromJson(value) : value;
		},
		put: function(key, value) {
			$sessionStorage[key] = angular.toJson(value);
		},
		remove: function(key) {
			delete $sessionStorage[key];
		}
	};
}]);